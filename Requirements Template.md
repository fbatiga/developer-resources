# 1. The Objective
This an intro to your PRD, where you'll give some background information to your reader to help them understand what the feature is. You can keep things at a high level and answer questions like:

    Who is this product/feature for?
    Which problem are you trying to solve?
    What is the goal of this feature?
    What will this feature do, and what will it not do?
    Having your "Objective" section is almost like having "big brother" watch over you — in a good way. It ensures that you are building out each feature relative to the high level goals that your product team must achieve. It also focuses your attention (and readers' attention) on the core product or feature.

    In essence, it helps you make sure that your team stays on track and avoids feature creep.

## 1.1 Context

## 1.2 Problem

## 1.3 Personas of the users (1 by profile type)


# 2. Core Components

Consider your PRD's Core Components section to be its Table of Contents. The Core Components section lists the key parts of the product feature. This helps readers understand what the rest of the PRD will focus on.

For example, let's say you are building out a new e-commerce feature to help your users buy packs of colored pencils. The core components for that PRD might include:

- Search Results Page (how your users will find the colored pencils they want)
- Product Detail Page (information about their favorite colored pencil)
- Checkout Page (the information they must provide in order to purchase that colored pencil)
- Order Confirmation Page (what happens after they purchase a colored pencil?)

In a way, your PRD is like a pyramid. You start off at the top of the pyramid, where you define which problem you are trying to solve. As you move down the pyramid (and the PRD), you go into more layers of detail on all the different components of each feature. This Core Components section helps your reader gain a high-level understanding of which pages will need to be built, and what is involved in building each page.

# 3. User Flow
The User Flow section is a space for you to define the consumer journey. There may be some overlap between this and the Core Components section. But only the User Flow section goes into detail about how the user gets through each step of the process. While the Core Components may lay out all the different pages and features which are involved, your User Flow is the step-by-step process that your users take in achieving their goal.

Going further on the colored pencil example, your user flow might be something like this:

example : 
## Home Page
- Sam, a lover of colored pencils, enters a phrase into the search box that takes him to the Search Results page
- Search Results Page
- Sam clicks on a blue colored pencil to get more information about it
- Product Detail Page
- Sam decides to buy this colored pencil and clicks, "Add to Cart"
- Sam then clicks, "Go to Checkout"
- Checkout Page
- Sam submits the form on the checkout page
- Order Confirmation Page
- Sam sees the order confirmation page
- Order Confirmation Email
- Sam receives an order confirmation email

For this section, we took the pages from the Core Components section and built out a few key actions from each of the pages. This explains what the user flow looks like from start to finish. Now, you can paint a picture of what this feature does and how each user can navigate the flow. You also gain a strong understanding of this new feature's key components.
Each element of a user flow must be clear and describe enough to constitute a sprint (or a list in Trello)


# 4. Details on Each User Flow Step
So far, you have defined your new feature's key objective, core components, and user flow. Now, it's time to define the specifics of each page. On each page, you will outline all elements of the feature you're working on — and all of the different states that may exist.

    First, you should list out all the different elements on the page. As you do so, remember the core goal of the product or feature that you're building. Any elements that you spec out should be directly related to that goal.

    Elements on the Product Detail Page
    Price
    Product name
    Description
    Sharing to social media
    Reviews
    Purchase
    Photos
    For each of the elements, you should go into more details to help your team understand what each component does, which states should be accounted for, and which type of development should be set forth.

## 4.1 Visual specification
    what does the feature look like (colors, sizes, positioning).
    Waht are the different visual states and interactions (hover, on click, disabled etc).

## 4.2 Behavior
    what does the feature do  (ex: on click this button will open a modal to xxx, see feature yyyy).
    What kind of controls and backound impacts. 
    What collateral impacts (Emails / notifications / status)
    

## 4.2 Backend specification
    Database or api impacts

Here is an example of the types of information you may need to provide, along with questions you should ask yourself (and answer) when writing the PRD:

    Photos
    There are several photos which can be cycled through in a slideshow
    When Sam clicks on a photo, he can see a larger photo of the pencil
    Price
    This is the price that Sam — the colored pencil enthusiast — pays for the pencil
    Product Name
    This is featured on the page
    Description
    How much of a description is written
    Is there a character or word count?
    Social Sharing
    Can this product page be shared to social media?
    For each type of social media (Facebook, Pinterest, Twitter, email, etc), what does the social post look like and say?
    Pro Tip: Think about who will be seeing it and what will convince them to click on the post
    Reviews
    What does this section look like when there are 0 reviews (empty state)?
    What does this section look like when there are 1 or more reviews (full state)?

As you go through this exercise when writing your PRD, remember its core goal — to get everyone on the same page so they can understand how the feature works. To accomplish this, you'll want to provide enough detail so that anyone who reads your PRD will have a full understanding of what the product does.
Each Detailed step must represent a trello card. As such it must make clear of `the what, the where, and the who`.

# 5. Platform, Testing and Deployment

All the devops related questions

# 6. Future Features
This section will help your team understand how the product may evolve over time. This doesn't need to be a hugely in-depth section, but it will help your team see the product or feature through your eyes.

Remember that as a product manager, you need to be its go-to expert. You likely have a lot more product knowledge than others, and you should share this knowledge to help your team get on the same page.

Never assume that others know what you do — and vice versa. The strongest step you can take as a product leader is to share your knowledge with your team.

Take the example of a contractor who is building a 50-story skyscraper. Perhaps you plan to start with a two-story building today, but in the future, you plan to add a lot more stories to that building. Your architects must put in the proper foundation to support that 50-story building in the future. Preparing today helps you succeed tomorrow. Scaling your product successfully involves planning for what will come next.