# Howto deploy to app store

- pull lastest code
- copy code to ios folder =>  `cordova prepare ios`
- open the xcode project => `open platforms/ios/<name of the app>.xcodeproj` . For some projects like **Tads** you'll need to open the workspace rather than the project `platforms/ios/<APP>.xcworkspace`
- run the app in emulator and test that the code is working
- Set the build target as **Generic IOS device**
- Click on the menu Product > Archive
- After the archiving is complete you'll see the Organiser window
- click on ** upload to the playstore **
- select the proper team (use ios credentials if you need to)
- Click on ùpload and wait for it to finish.