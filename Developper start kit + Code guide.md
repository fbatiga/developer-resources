# START KIT

Accounts that need to be created :

- Email 

- Access to the dev env 
- Access to gitlab (group or per project basis)
- Access to documentation (Mot de passe http : adminarea / area51dansle93)
- Access to Slack
- Access to Trello
- Access to jenkins


# Tools 

- Trello for tasks management. Remember to add a picture so we can recognize you.
- Slack for commicating with the team. First thing when you start working, say hi in the general channel.When you are leaving say goodbye.
- Daily emails for tracking the tasks that you did during the day.


# Coding conventions 

For PHP : 
http://symfony.com/doc/current/contributing/code/standards.html

For javascript : 
https://github.com/airbnb/javascript

For git : https://seesparkbox.com/foundry/semantic_commit_messages

Here are some good advice on how to write a git message.

https://chris.beams.io/posts/git-commit/


## General rules 

### 0 the basics

- please code like if it was your own website. Do no leave loose ends.
- Do not leave code as lorem ipsum
- Don't wait for us to tell you what to do. If you see something that needs to be fixed, fix it. If you are not sure ask.
- If there is some lorem ipsum code or some static content in a page, connect it or ask if it need to be connected.
- remove commented code
- add comments
- Follow design patterns.
- Clean console.log in your code.
- create meaningful variables names
- never leave loose ends (promises without catch, subscribe without error function)
- Try to inform the user as much as possible / don't leave silent errors

>this.contactEndpoint.delete({ _id: currentId }).$observable.subscribe(res => {
>        console.log(res);
>        this.loadItems();
>      }, err => { console.log(err) });      //// @fixme don't leave silent errors. You need to warn the user

- When i give you feedbacks on code style try to apply those feedbacks to all future features on the project.
- watch out for typos mistakes in your code :

> disiplineFilter / slectValue / 

- keep the code readable 
- keep your code consistent and easy to read :

```
              <div class="card__buttons" *ngIf="examenPreview.hasAccess == false && examenPreview.credits>userProfile.credits">
                <a (click)="requirePass()" class="btn price">{{ examenPreview.credits}} ECNiPass</a>
              </div>
              <div class="card__buttons" *ngIf="examenPreview.hasAccess == false && userProfile.credits>=examenPreview.credits">
                <a (click)=chargeEcniPass() class="btn price">{{ examenPreview.credits}} ECNiPass</a>
              </div>
```
keep variables on the same side. add spaces between operators
### 1 - Avoid duplicating code 
This is the most important rule. If you write or copy paster the same piece of code two times, think about refactoring that piece of code. If you do 3 times you definitely have to refactor it .
Code duplication is the one thing we hate the most at Enyosolutions.  It's the cancer of programming. It causes the pain of fixing the same bug in 20 different places.

### 2 - be wary of copy paste
With stack overflow around, it's easy and time saving to google the issue you have and copy paste the solution. Inherently it's not a bad thing, we all do it.

But if you don't take the time to understand what you are copying, you'll not progress as a programmer. Worse yet, you might copy code that is incoherent / not relevant to your problem.

So take the time to read and understand the code that you are copying. Then modify the code before pasting.

Also when copying a code, rename the variables to make sense in the context of the file you are pasting the code into.

### 3 - The solution in not always in the first answer of stack overflow

Don't just go with the first answer read all of them. Also often with a plugin or a framework, you'll find help in the github bugtracker or the forum of the framework (ionic for example).

### 3 - respect code formating rules 

When i open piece of code i want to see :
- camel case variables
- comments
- curly braces around all controls statements
- no funky or weird variable names


### 4 - cleaning after yourself

Before you commit a code, please take the time to :

- remove debug log (`var_dump`, `console.log` etc)
- Remove commented code. We don't need commented code. Old code can be retrieved using the git history. When you are coding, it makes sense to comment the old code, but ultimately when you are finish you have to delete the old code.


### 5 jquery performance 

read here
https://learn.jquery.com/performance/
and here 
https://code.tutsplus.com/tutorials/10-ways-to-instantly-increase-your-jquery-performance--net-5551


### 6 details

#### 6.1  avoid if statements on one lines and without curly braces


```
// bad
if (test)
  return false;

// bad
if (test) return false;

// good
if (test) {
  return false;
}

// bad
function foo() { return false; }

// good
function bar() {
  return false;
}
```

#### 6.2 put a space after the commas 

```
// bad
function display(a,b,c)

// good
function display(a, b, c)
```

#### 6.3 put lines breaks after opening curly braces

```
// bad
if (test) {
  thing1();
  thing2();
}
else {
  thing3();
}

// good
if (test) {
  thing1();
  thing2();
} else {
  thing3();
}
```

#### 6.4 Prefix your comments with @FIXME or @TODO or @NOTE or @DELETE

Prefixing your comments with FIXME or TODO helps other developers quickly understand if you're pointing out a problem that needs to be revisited, or if you're suggesting a solution to the problem that needs to be implemented. These are different than regular comments because they are actionable. The actions are FIXME: -- need to figure this out or TODO: -- need to implement.

##### 6.4.1 Use // @FIXME: to annotate problems.
```
class Calculator extends Abacus {
  constructor() {
    super();

    // FIXME: shouldn’t use a global here
    total = 0;
  }
}

```

##### 6.4.2 Use // @TODO: to annotate solutions to problems.

```
class Calculator extends Abacus {
  constructor() {
    super();

    // TODO: total should be configurable by an options param
    this.total = 0;
  }
}
```


##### 6.4.2 Use // @DELETE: to annotate piece of code that must be deleted.

class Calculator extends Abacus {
  constructor() {
    super();

    // @DELETE: total is now useless
    // this.total = 0;
  }
}
```

